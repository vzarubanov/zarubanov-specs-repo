#
# Be sure to run `pod lib lint Toolkit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'Toolkit'
    s.version          = '0.1.0'
    s.summary          = 'Toolkit framework'

    s.description      = <<-DESC
    Most common components
    DESC

    s.homepage         = 'https://vzarubanov@bitbucket.org/vzarubanov/zarubanov-toolkit.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Vasil Zarubanau' => 'zarubanov.vasily@gmail.com' }
    s.source           = { :git => 'https://vzarubanov@bitbucket.org/vzarubanov/zarubanov-toolkit.git', :tag => s.version.to_s }

    s.ios.deployment_target = '11.0'
    #s.watchos.deployment_target = '3.0'
    s.swift_version = '5.0'
    s.frameworks = 'Foundation'
    s.dependency 'ReactiveSwift', '~> 6.0.0'
    s.dependency 'ReactiveCocoa', '~> 10.0.0'
    s.dependency 'PKHUD', '~> 5.0'

end
